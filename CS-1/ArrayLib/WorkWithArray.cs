﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


   public class WorkWithArray
    {
    private int[] array;
     public WorkWithArray(int startIndex,int endIndex,int step)
        {
        //вычислим длину требуемого массива
        int arrayLenth = Math.Abs(startIndex) + Math.Abs(endIndex);
        array = new int[arrayLenth+1];
        //вычислим шаг с учетом того, в какую сторону двигаться по циклу
        //step = Math.Abs(step) *  ((startIndex > endIndex) ?  1:-1);
        int value = startIndex;
        for (int i = 0; i <= arrayLenth; i += step)
        {
            
            //Console.WriteLine(value);
            array[i] = value;
            value += step;
        }
        }
     public int Summ { get
        {
            int summ = 0;
            foreach (var x in array)
            {
                summ += x;
            }
            return summ;
        } }
    public void Multi(int value)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = value*array[i];
        }
    }
    public int MaxCount(int max)
    {
        int summ = 0;
        foreach (var x in array)
        {

            if (x > summ)
            {
                Console.WriteLine(x);
                summ++;
                
            }
         }
        return summ;
    }
     public int[] Inverse()
    {
        int[] newArray = new int[array.Length];
        array.CopyTo(newArray, 0);
        for (int i = 0; i < newArray.Length; i++)
        {
            newArray[i] = -newArray[i];
        }
        return newArray;
    }
    }

