﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Lesson8
{
    class Program
    {
        //1. С помощью рефлексии выведите все свойства структуры DateTime.
        static void Task1()
        {
            DateTime time = new DateTime();
            var props = time.GetType().GetProperties();
            foreach (var pr in props)
            {
                Console.WriteLine(pr.Name);
            }

        }
        //2. Создайте простую форму на котором свяжите свойство Text элемента TextBox со свойством Value элемента NumericUpDown.
        static void Task2()
        {
            FormTask2 form = new FormTask2();
            Application.Run(form);

        }
        static void Main(string[] args)
        {
            Task2();
            //Console.ReadKey();
        }
    }
}
