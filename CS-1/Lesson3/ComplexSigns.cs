﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public class ComplexSigns
    {
        double im;
        double re;

        ComplexSigns(double re,double im)
        {
            this.re = re;
            this.im = im;
        }
        ///добавим методы для сложения-вычитания-умножения
        public ComplexSigns Multi(ComplexSigns x)
        {
            return new ComplexSigns(re * x.re - re * x.im, re * x.im + im * x.re);
        }
        public ComplexSigns Minus(ComplexSigns x)
        {
            return new ComplexSigns(re - x.re, im - x.im);
        }
        public ComplexSigns Plus(ComplexSigns x)
        {
            return new ComplexSigns(re + x.re, im + x.im);
        }

        /// переопределил операторы
        public static ComplexSigns operator * (ComplexSigns a,ComplexSigns b)
        {
            return new ComplexSigns(a.re*b.re-a.re*b.im,a.re*b.im+a.im*b.re);
        }
        public static ComplexSigns operator +(ComplexSigns a,ComplexSigns b)
        {
            return new ComplexSigns(a.im + b.im, a.re + b.re);
        }
        public static ComplexSigns operator -(ComplexSigns a, ComplexSigns b)
        {
            return new ComplexSigns(a.im - b.im, a.re - b.re);
        }

        public override string ToString()
        {
            return re + "+" + im + "i";
        }
    }
}
