﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public class Fraction
    {
        public int A { get; private set; }//числитель
        private int _b;
        //поидее так более правильно делать проверку. но не понятно почему без дублирующего поля переполнение стека идет
        public int B 
        { 
            get { return _b; }
            private set
            {
                if (value == 0)
                {
                    throw new ArgumentException("Знаменатель не может быть равен 0");
                }
                _b = value;
            }
        }

        public Double Value 
             { 
                 get
                     {
                         return (Double)A / (Double)B;
                     } 
             }


        public Fraction(int A,int B)
        {
            this.A = A;
            //проверка без сеттера
           //if (B == 0) throw new ArgumentException("Делитель не может быть равен 0");
            this.B = B;
        }
        public override string ToString()
        {
            return A + "/" + B;
        }
        ///сложение-вычитание-умножение-деление дробей решил сделать через операторы
        public static Fraction operator +(Fraction one,Fraction two)
        {
            return new Fraction(one.A * two.B + two.A * one.B, one.B * two.B);
        }
        public static Fraction operator -(Fraction one,Fraction two)
        {
            return new Fraction(one.A * two.B - two.A * one.B, one.B * two.B);
        }
        public static Fraction operator *(Fraction one,Fraction two)
        {
            return new Fraction(one.A * two.A, one.B * two.B);
        }
        public static Fraction operator /(Fraction one, Fraction two)
        {
            return new Fraction(one.A * two.B, one.B * two.A);
        }
        /// <summary>
        /// без нахождения наибольшего общего делителя
        /// </summary>
        public Fraction Simplification()
        {
            int max = A;
            if (A<B)
            {
                max = B;
            }
            int del = 1;

            for (int i = B; i >= 1; i--)
            {
                if (A % i==0 & B % i == 0)
                {
                    A = A / i;
                    B = B / i;
                }
            }
            return this;
        }
    }
}
