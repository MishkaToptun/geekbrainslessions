﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    class Program
    {
        struct Complex
        {
            public double im;
            public double re;
            //  в C# в структурах могут храниться также действия над данными
            public Complex Plus(Complex x)
            {
                Complex y;
                y.im = im + x.im;
                y.re = re + x.re;
                return y;
            }
            //  Пример произведения двух комплексных чисел
            public Complex Multi(Complex x)
            {
                Complex y;
                y.im = re * x.im + im * x.re;
                y.re = re * x.re - im * x.im;
                return y;
            }
            public Complex Minus(Complex x)
            {
                Complex y;
                y.im = im - x.im;
                y.re = re - x.re;
                return y;
            }
            /// тоже интересная реализация как перегрузка операторов (погуглил)
            public static Complex operator +(Complex a, Complex b)
            {
                return new Complex { im = a.im + b.im, re = a.re + b.re };
            }
            public static Complex operator -(Complex a, Complex b)
            {
                return new Complex { im = a.im - b.im, re = a.re - b.re };
            }
            public override string ToString()
            {
                return re + "+" + im + "i";
            }
        }

        static void Task1()
        {

        }

        //2.а) С клавиатуры вводятся числа, пока не будет введён 0 (каждое число в новой строке). 
        //Требуется подсчитать сумму всех нечётных положительных чисел.Сами числа и сумму вывести на экран, используя tryParse.
        static void Task2()
        {
            bool flag = true;
            int sign = 0;
            int summ = 0;
            while (flag)
            {
                Console.WriteLine("Введите число, 0 -для выхода");
                
                bool testSing = int.TryParse(Console.ReadLine(), out sign);
                if (testSing==true)
                {
                    if (sign == 0) break;
                    if (sign % 2 != 0 & sign>0)
                    {
                        summ += sign;
                    }
                    
                }
                else
                {
                    Console.WriteLine("Нужно ввести простое целове число, 0 - для выхода");
                }
            }
            Console.WriteLine("Сумма нечетных положительных чисел=" + summ);
            Console.ReadKey();
        }
        static void Task3()
        {
            Fraction one = new Fraction(4,8);


            Fraction two = new Fraction(12, 0);
            Console.WriteLine("Исходные дроби:" + one + " " + two);
            one.Simplification();
            two.Simplification();
            Console.WriteLine("После упрощения:" + one + " " + two);
            Console.WriteLine("сумма = " + (one + two).Simplification());
            Console.WriteLine("разница = " + (one - two).Simplification());
            Console.WriteLine("произведение = " + (one * two).Simplification());
            Console.WriteLine("деление = " + (one / two).Simplification());
            
            Console.ReadKey();
        }
        static void Main(string[] args)
        {
            Task3();
        }
    }
}
