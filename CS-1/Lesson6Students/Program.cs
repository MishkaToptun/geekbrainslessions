﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
class Program
{
    static int MyDelegat(Student st1, Student st2)          // Создаем метод для сравнения для экземпляров
    {

        return String.Compare(st1.firstName, st2.firstName);          // Сравниваем две строки
    }
    static int AgeCompare(Student st1,Student st2)
    {
        if (st1.age < st2.age) return -1;
        if (st1.age > st2.age) return 1;
        return 0;
    }
    //3. Переделать программу Пример использования коллекций для решения следующих задач:
    //а) Подсчитать количество студентов учащихся на 5 и 6 курсах;
    //б) подсчитать сколько студентов в возрасте от 18 до 20 лет на каком курсе учатся(*частотный массив);
    //в) отсортировать список по возрасту студента;
    //г) *отсортировать список по курсу и возрасту студента;



    static void Main(string[] args)
    {
        int course5 = 0;
        int course6 = 0;
        List<Student> list = new List<Student>();
        Dictionary<int, Dictionary<int, int>> studens = new Dictionary<int, Dictionary<int, int>>() {
            {1,new Dictionary<int, int>()
        {
            {18,0},{19,0},{20,0}
        }    },
            {2,new Dictionary<int, int>()
        {
            {18,0},{19,0},{20,0}
        }    },
            {3,new Dictionary<int, int>()
        {
            {18,0},{19,0},{20,0}
        }   },
            { 4,new Dictionary<int, int>()
        {
            {18,0},{19,0},{20,0}
        }   },
            {5,new Dictionary<int, int>()
        {
            {18,0},{19,0},{20,0}
        }   },
            {6,new Dictionary<int, int>()
        {
            {18,0},{19,0},{20,0}
        }   }};
         
        DateTime dt = DateTime.Now;
        StreamReader sr = new StreamReader("studentsinfo.csv");
        //чтение из файла
        while (!sr.EndOfStream)
        {
            try
            {
                string[] s = sr.ReadLine().Split(';');
                var st = new Student(s[0], s[1], s[2], s[3], s[4], int.Parse(s[6]), int.Parse(s[5]), int.Parse(s[7]), s[8]);
                //подсчитаем количество студентов на 5 и 6 курсах
                if (st.course ==5) course5++;
                if (st.course == 6) course6++;
                list.Add(st);
                if (studens.ContainsKey(st.course))
                {
                    var stud = studens[st.course];
                    if (stud.ContainsKey(st.age))
                    {
                        stud[st.age] = stud[st.age] + 1;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Ошибка!ESC - прекратить выполнение программы");
                // Выход из Main
                if (Console.ReadKey().Key == ConsoleKey.Escape) return;
            }
        }
        sr.Close();

        foreach (var pair in studens)
        {
            Console.WriteLine("Курс: " + pair.Key);
            foreach (var st in pair.Value)
            {
                Console.WriteLine("Возраст: {0} Количество студентов {1}", st.Key, st.Value);
            }
        }
        
         
        list.Sort(new Comparison<Student>(AgeCompare));
        
        Console.WriteLine("Всего студентов:" + list.Count);
        Console.WriteLine("На 5 курсе:{0}", course5);
        Console.WriteLine("На 6 курсе:{0}", course6);
       // foreach (var v in list) Console.WriteLine(v.firstName);
        Console.WriteLine(DateTime.Now - dt);
        Console.ReadKey();
    }
}

