﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    class Program
    { 
        //1. Изменить программу вывода таблицы функции так, чтобы можно было передавать функции типа double (double, double). Продемонстрировать работу на функции с функцией a* x^2 и функцией a* sin(x).
        #region задача 1
        public delegate double Fun(double x, double y);
        ///функция вывода на экран значения принимаемой функции
        public static void Table(Fun F, double x,double y, double a,double b)
        {
            Console.WriteLine("----- X ----- Y -----F-----");
            double startY = y;
            while (x <= a)
            {
                while (y <= b)
                {
                    Console.WriteLine("| {0,8:0.000} | {1,8:0.000} | {2,8:0.000} |", x,y,F(x, y));
                    y++;
                }
                x+=1;
                y = startY;
            }
            Console.WriteLine("---------------------");
        }
        public static double MyFunc2(double x,double y)
        {
            return x * Math.Sin(y);
        }
        public static double MyFunc1(double x,double y)
        {
            return x * Math.Pow(y, 2);
        }
        static void Task1()
        {
            Table(MyFunc1, 0, 0, 5, 5);
            Table(MyFunc2,0, 0, 2, 2);
        }
        #endregion
        //2. Модифицировать программу нахождения минимума функции так, чтобы можно было передавать функцию в виде делегата.
        //а) Сделать меню с различными функциями и представить пользователю выбор, для какой функции и на каком отрезке находить минимум.Использовать массив(или список) делегатов, в котором хранятся различные функции.
        //б) *Переделать функцию Load, чтобы она возвращала массив считанных значений.Пусть она возвращает минимум через параметр (с использованием модификатора out).
        #region задача 2
        public static double MyFun(double x)
        {
            return x * x - 50 * x + 10;
        }
        public delegate double FunDelegate(double x);

         
        static double Minimum(FunDelegate F,double a,double b,double h)
        {
            double minimum = F(a); ;
            while (a <= b)
            {
                if (minimum > F(a))
                {
                    minimum = F(a);
                }
                a += h;
            }
            return minimum;

        }


        static void Task2()
        {
            List<FunDelegate> MyFunctions = new List<FunDelegate>();
            MyFunctions.Add(Math.Sin);
            MyFunctions.Add(Math.Cos);
            MyFunctions.Add(Math.Tan);
            Console.WriteLine("Введите номер функции, и участок, на котором требуется найти минимум\n 1-sin, 2-cos,3-tan");
            var number = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите начало и конец отрезка:");
            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());
            Console.WriteLine("Minimum: " + Minimum(MyFunctions[number], a, b, 0.1));
        }
        public static double[] Load(string fileName,out double minimum)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader bw = new BinaryReader(fs);
            double min = double.MaxValue;
            double d;
            List<double> values = new List<double>();
            for (int i = 0; i < fs.Length / sizeof(double); i++)
            {
                // Считываем значение и переходим к следующему
                d = bw.ReadDouble();
                values.Add(d);
                if (d < min) min = d;
            }
            bw.Close();
            fs.Close();
            minimum = min;
            return values.ToArray();
        }
        #endregion




        static void Main(string[] args)
        {
            Task2();

            Console.ReadKey();
        }
    }
}
