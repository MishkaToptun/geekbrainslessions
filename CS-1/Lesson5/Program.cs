﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lesson5
{
    class Program
    {
        //1. Создать программу, которая будет проверять корректность ввода логина.
        //    Корректным логином будет строка от 2 до 10 символов, 
        //    содержащая только буквы латинского алфавита или цифры, 
        //    при этом цифра не может быть первой:
        //а) без использования регулярных выражений;
        //б) **с использованием регулярных выражений.

        //проверка без использования регулярных выражений
        static bool CheckLogin(string login)
        {
            //если первый символ - число
            if (char.IsDigit(login[0])) return false;
            //проверка логина по длине
            if (login.Length < 2 | login.Length > 10) return false;
            bool flag = true;
            foreach (var sym in login)
            {
                 
                if (sym >= 97 & sym <= 122)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                    break;
                }
            }

            return flag;
        }
        //проверка с использованием регулярных выражений
        static bool CheckLoginReg(string s)
        {
            //проверка логина по длине
            if (s.Length < 2 | s.Length > 10) return false;
            //найдем 1 символ или любая буква на кириллице
            string pattern = @"^\d|[а-яА-Я]";
            RegexOptions options = RegexOptions.Multiline;
            if (Regex.Matches(s, pattern, options).Count > 0)
                return false;

            return true;
        }
        static void Task1()
        {
            string login = "ssd1sdsdЯ";
            Console.WriteLine(CheckLoginReg(login));
        }
        static void Task2()
        {
            string test = "one one two to two two one three один два три два два два три  1234456 fffddww1w wsssddaa asasd wwewer  weeekjkjd kjkjiujj pp e[p[ sds четырехступенчатый четфыфыфыфыфычатый";
            Message.message = test;
            Message.PrintLongWords(6);
            Message.DeleteWithSymbolInEnd((char)"w"[0]);
            Console.WriteLine("максимальное слово по длине: " + Message.MaxWord());
            Console.WriteLine("Строка из максимальных строк:");
            Console.WriteLine(Message.MaxWords());
        }
        //3. *Для двух строк написать метод, определяющий, является ли одна строка перестановкой другой.
        //Например: badc являются перестановкой abcd.
        static bool TwinString(string A,string B)
        {
            int lenghtA = A.Length;
            int lenghtB = B.Length;
            if (lenghtA != lenghtB) return false;
            foreach (var a in A)
            {
                int index = 0;
                foreach (var b in B)
                {
                    if (a == b)
                    {
                        index++;
                    }
                }
                if (index > 1 | index == 0)
                {
                    return false;
                }
            }
            return true;
        }
        static void Task3()
        {
            string A = "abcde";
            string B = "1cdea";
            Console.WriteLine(TwinString(A, B));

        }

        static void Main(string[] args)
        {
            Task3();
            Console.ReadKey();
        }
    }
}
