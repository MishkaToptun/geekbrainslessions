﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

//2.Разработать статический класс Message, содержащий следующие статические методы для обработки текста:
//а) Вывести только те слова сообщения, которые содержат не более n букв.
//б) Удалить из сообщения все слова, которые заканчиваются на заданный символ.
//в) Найти самое длинное слово сообщения.
//г) Сформировать строку с помощью StringBuilder из самых длинных слов сообщения.
//д) ***Создать метод, который производит частотный анализ текста. В качестве параметра в него передается массив слов и текст, 
//    в качестве результата метод возвращает сколько раз каждое из слов массива входит в этот текст. Здесь требуется использовать класс Dictionary.
 public static class Message
{
     //по логике - в данном классе будет содержаться строка и при любом использовании 
    public static string message;
    public static void PrintLongWords(int n)
    {
        
        //найдем слова разделенные пробелами
        string pattern = @"\S+";
        RegexOptions options = RegexOptions.Multiline;
        //осуществим прохождение по найдемнным словам и напечатаем те у которых длина меньше n
        foreach (var x in Regex.Matches(message, pattern, options))
        {
            if (x.ToString().Length<=n)
            {
                Console.WriteLine(x);
            }
        }
    }
    public static void DeleteWithSymbolInEnd(char end)
    {
        //найдем слова разделенные пробелами
        string pattern = @"\S+";
        RegexOptions options = RegexOptions.Multiline;
        //осуществим прохождение по найдемнным словам и напечатаем те у которых длина меньше n
        Console.WriteLine("Старая строка: " + message);
        foreach (var x in Regex.Matches(message, pattern, options))
        {
            if ((char)x.ToString()[x.ToString().Length-1]==end)
            {
                message= message.Replace(x.ToString(), "");
            }
        }
        Console.WriteLine("Новая строка: " + message);
    }
    public static string MaxWord()
    {
        string max = "";
        string pattern = @"\S+";
        RegexOptions options = RegexOptions.Multiline;
        foreach (var x in Regex.Matches(message, pattern, options))
        {
            if (x.ToString().Length > max.Length)
            {
                max = x.ToString();
            }
        }
        return max;
    }
    //создадим строку из  строк максимальной длины
    public static string MaxWords()
    {
        int max = MaxWord().Length;

        List<string> list = new List<string>();
        string pattern = @"\S+";
        RegexOptions options = RegexOptions.Multiline;
        StringBuilder sb = new StringBuilder();
        string result = "";
        foreach (var x in Regex.Matches(message, pattern, options))
        {
            if (x.ToString().Length >= max)
            {
                sb.Append(x.ToString());
            }
        }
        return sb.ToString();
         
   }


}

