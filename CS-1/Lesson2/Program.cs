﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
    class Program
    {
        //Глухов Виктор 
        //Задание 1
        //Написать метод, возвращающий минимальное из трёх чисел.
        static void Task1()
        {
            Console.WriteLine("Введите 1ое число");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите 2ое число");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите 3ое число");
            int c = int.Parse(Console.ReadLine());

            Console.WriteLine($"Минимальное число: {MinOf3(a, b, c)}");
            Console.ReadKey();
            int MinOf3(int one,int two, int three)
            {

                if (one > two)
                {
                    one = two;
                }
                if (one < three)
                {
                    return one;
                }
                else
                {
                    return three;
                }
             
            }
        }
        //Глухов Виктор
        //Задание 2
        //Написать метод подсчета количества цифр числа.
        static void Task2()
        {
            Console.WriteLine("Введите целое число:");
            int data = int.Parse(Console.ReadLine());
            string symbols = data.ToString();
            int count = symbols.Length;
            Console.WriteLine("Количество цифр = " + count);
            Console.ReadKey();
        }
        //Глухов Виктор
        //Задание 3
        //С клавиатуры вводятся числа, пока не будет введен 0. Подсчитать сумму всех нечетных положительных чисел.

        static void Task3()
        {
            bool flag = true;
            int summ = 0;

            while (flag)
            {
                Console.WriteLine("Введите целое число или 0 чтобы завершить");
                try
                {
                    int var = int.Parse(Console.ReadLine());
                    if (var == 0) break;
                    if (var > 0 & var%2 != 0)
                    {
                        summ++;
                    }
                }
                catch
                {
                    Console.WriteLine("Неверный формат данных, нужно вводить только целые числа");
                }
            }
            Console.WriteLine("Сумма нечетных =" + summ);
            Console.ReadKey();
        }
        //Глухов Виктор
        //4. Реализовать метод проверки логина и пароля.На вход метода подается логин и пароль. 
        //На выходе истина, если прошел авторизацию, и ложь, если не прошел (Логин: root, Password: GeekBrains). 
        //Используя метод проверки логина и пароля, написать программу: пользователь вводит логин и пароль, программа
        //пропускает его дальше или не пропускает.С помощью цикла do while ограничить ввод пароля тремя попытками.
        static void Task4()
        {
            int count = 3;
            string login = "";
            string password = "";
            do
            {
                count--;
                Console.WriteLine("Введите логин");
                login = Console.ReadLine();
                Console.WriteLine("Введите пароль");
                password = Console.ReadLine();
                if (CheckPass(login, password))
                {
                    Console.WriteLine("Пользователь верный");
                    break;
                }
                else
                {
                    if (count >= 1)
                    {
                        Console.WriteLine($"Имя пользователя или пароль неверные, осталось попыток: {count}");
                    }
                    else
                    {
                        Console.WriteLine("Количество попыток исчерпано!");
                    }
                }
                
            } while (count >= 1);
            Console.ReadKey();
        }
        static bool CheckPass(string login,string password)
        {
            if (login == "root" & password == "GeekBrains")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Глухов Виктор
        //5.а) Написать программу, которая запрашивает массу и рост человека, 
        //вычисляет его индекс массы и сообщает, нужно ли человеку похудеть, набрать вес или всё в норме.
        //б) *Рассчитать, на сколько кг похудеть или сколько кг набрать для нормализации веса.
        static void Task5()
        {
            Console.WriteLine("Введите ваш рост, см");
            int height = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите ваш вес, кг");
            int mass = int.Parse(Console.ReadLine());
            //использовано приведение так как при делении целый чисел происходит округление
            double index = mass / Math.Pow((double)height / 100, 2);
            Console.WriteLine($"Индекс массы тела {index}");
            if (index <= 16)
            {
                Console.WriteLine("Выраженный дефицит массы тела");
            } 
            else if (index <= 18.5)
            {
                Console.WriteLine("Недостаточная масса тела");
            }
            else if (index <= 25)
            {
                Console.WriteLine("Норма");
            }
            else if (index <= 30)
            {
                Console.WriteLine("Избыточная масса тела");
            }
            else if (index <= 35)
            {
                Console.WriteLine("Ожирение 1 степени");
            }
            else if (index <= 40)
            {
                Console.WriteLine("Ожирение 2 степени");
            }
            else 
            {
                Console.WriteLine("Ожирение 3 степени");
            }
            Console.ReadKey();
        }
        static void Task7()
        {
            Console.WriteLine("Введите a:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите b:");
            int b = int.Parse(Console.ReadLine());
            PrintAB(a, b);
            Console.WriteLine(SummAB(a, b));
            Console.ReadKey();
            
        }
        static void PrintAB(int a, int b)
        {
            if (a <= b)
            {
                Console.WriteLine(a);
                PrintAB(a + 1, b);
            }
        }
        static int SummAB(int a,int b,int summ=0)
        {
            
            if (a <= b)
            {
                summ += a;
                 
                return SummAB(a+1, b,summ);
            }
            else
            {
                return summ;
            }
            
        }
        static void Main(string[] args)
        {
            Task7();
         }
    }
}
