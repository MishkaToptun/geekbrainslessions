﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    class Program
    {
        //1. Дан целочисленный массив из 20 элементов.Элементы массива могут принимать целые значения от –10 000 до 10 000 включительно.
        //    Заполнить случайными числами.Написать программу, позволяющую найти и вывести количество пар элементов массива, 
        //    в которых только одно число делится на 3. В данной задаче под парой подразумевается два подряд идущих элемента массива.
        static void Task1()
        {
            int[] PairsArray = new int[20];
            //заполнение массива
            var rand = new Random();
            for (int i=0; i<20; i++)
            {
                var sing = rand.Next(-10000, 10000);
                PairsArray[i] = sing;
            }
            int pairsSumm = 0;
            for (int i = 0; i< 20; i=i+2)
            {
                Console.WriteLine(i);
                if (PairsArray[i]%3==0 | PairsArray[i + 1] % 3 == 0)
                {
                    Console.WriteLine("пара: " + PairsArray[i] + " " + PairsArray[i + 1]);
                    pairsSumm++;
                }
            }
            Console.WriteLine("количество пар" + pairsSumm);
        }
//        2. Реализуйте задачу 1 в виде статического класса StaticClass;
//         а) Класс должен содержать статический метод, который принимает на вход массив и решает задачу 1;
//        б) Добавьте статический метод для считывания массива из текстового файла.Метод должен возвращать массив целых чисел;
//        в)*Добавьте обработку ситуации отсутствия файла на диске.
        static class StaticClass
        {
             public static void TestArray(int[] array)
            {
                int summ = 0;
                if (array != null)
                {
                    for (int i = 0; i < array.Length; i = i + 2)
                    {
                        Console.WriteLine(i);
                        if (array[i] % 3 == 0 | array[i + 1] % 3 == 0)
                        {
                            Console.WriteLine("пара: " + array[i] + " " + array[i + 1]);
                            summ++;
                        }
                    }
                    Console.WriteLine("кол-во пар: " + summ);
                }
                else
                {
                    throw new ArgumentNullException() ;
                }
            }
            public static int[] ReadFromFile(string filename)
            {
                List<int> list = new List<int>();
                if (File.Exists(filename))
                {
                    StreamReader stream = new StreamReader(filename);

                    while (!stream.EndOfStream)
                    {
                        string s = stream.ReadLine();
                        try
                        {
                            list.Add(int.Parse(s));
                        }
                        catch (Exception exc)
                        {
                            throw new Exception("файл не найден");
                           // Console.WriteLine(exc.Message);
                        }

                    }
                    stream.Close();
                    return list.ToArray();
                }
                else
                {
                    Console.WriteLine("Файл не найден");
                    return null;
                }
                return null;
            }

        }
        //3.
        //а) Дописать класс для работы с одномерным массивом.Реализовать конструктор, создающий массив определенного размера и 
        ////заполняющий массив числами от начального значения с заданным шагом.Создать свойство Sum, которое возвращает сумму элементов массива, 
        //метод Inverse, возвращающий новый массив с измененными знаками у всех элементов массива(старый массив, остается без изменений), 
        //метод Multi, умножающий каждый элемент массива на определённое число, свойство MaxCount,
        //возвращающее количество максимальных элементов.
        //б)** Создать библиотеку содержащую класс для работы с массивом.Продемонстрировать работу библиотеки
        //в) *** Подсчитать частоту вхождения каждого элемента в массив(коллекция Dictionary<int, int>)




        static void Task2()
        {
            int[] PairsArray = new int[20];
            //заполнение массива
            var rand = new Random();
            for (int i = 0; i < 20; i++)
            {
                var sing = rand.Next(-10000, 10000);
                PairsArray[i] = sing;
            }
            StaticClass.TestArray(PairsArray);
            var array = StaticClass.ReadFromFile("test.txt");

                StaticClass.TestArray(array);

            

        }
        static void Task3()
        {
            WorkWithArray myArray = new WorkWithArray(-10,10,3);
            Console.WriteLine(myArray.MaxCount(10));

        }
        static void Main(string[] args)
        {
            Task3();
            Console.ReadKey();
        }
    }
}
