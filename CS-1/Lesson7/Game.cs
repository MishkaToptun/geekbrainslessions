﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Game
{
    //текущее значение числа
    int _currentCount;
    Stack<int> stack;
    public int currentCount {
        get { return _currentCount; } 
        private set { count++; _currentCount = value; } }
    //количество шагов
    public int count { get; private set; }
    //загадываемое число
    public int mainCount { get; private set; }

    public Game(int a,int b)
    {
        var value = new Random().Next(a, b);
        currentCount = 1;
        count = 0;
        mainCount = value;
        stack = new Stack<int>();
    }

    public void ResetGame()
    {
        currentCount = 1;
        count = 0;
        stack = new Stack<int>();
    }
    public void Plus(int value = 1)
    {
        stack.Push(currentCount);
        currentCount = currentCount + value;
        
    }
    public void Multi(int value = 2)
    {
        stack.Push(currentCount);
        currentCount = currentCount * value;
     
    }
    public void Back()
    {
        if (count > 0)
        {
            currentCount = stack.Pop();
            count -= 2;
        }
    }


}

