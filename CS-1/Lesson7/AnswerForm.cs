﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson7
{
    public partial class AnswerForm : Form
    {
        FormAnswers form;
        public AnswerForm(FormAnswers form)
        {
            InitializeComponent();
            this.form = form;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            form.TestAnswer(this.textBox1.Text);
            this.Close();
        }
    }
}
