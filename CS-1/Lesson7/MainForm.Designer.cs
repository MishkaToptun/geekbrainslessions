﻿
namespace Lesson7
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.multiButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.numberLabel = new System.Windows.Forms.Label();
            this.numberNow = new System.Windows.Forms.Label();
            this.countLabel = new System.Windows.Forms.Label();
            this.newGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(129, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "+1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // multiButton
            // 
            this.multiButton.Location = new System.Drawing.Point(129, 98);
            this.multiButton.Name = "multiButton";
            this.multiButton.Size = new System.Drawing.Size(75, 23);
            this.multiButton.TabIndex = 1;
            this.multiButton.Text = "*2";
            this.multiButton.UseVisualStyleBackColor = true;
            this.multiButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(129, 127);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Назад";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(129, 156);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 3;
            this.resetButton.Text = "Сброс";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // numberLabel
            // 
            this.numberLabel.AutoSize = true;
            this.numberLabel.Location = new System.Drawing.Point(9, 33);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Size = new System.Drawing.Size(114, 13);
            this.numberLabel.TabIndex = 4;
            this.numberLabel.Text = "загадываемое число";
            // 
            // numberNow
            // 
            this.numberNow.AutoSize = true;
            this.numberNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberNow.Location = new System.Drawing.Point(3, 69);
            this.numberNow.Name = "numberNow";
            this.numberNow.Size = new System.Drawing.Size(69, 73);
            this.numberNow.TabIndex = 5;
            this.numberNow.Text = "1";
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Location = new System.Drawing.Point(13, 166);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(97, 13);
            this.countLabel.TabIndex = 6;
            this.countLabel.Text = "количество ходов";
            // 
            // newGame
            // 
            this.newGame.Location = new System.Drawing.Point(129, 40);
            this.newGame.Name = "newGame";
            this.newGame.Size = new System.Drawing.Size(75, 23);
            this.newGame.TabIndex = 7;
            this.newGame.Text = "Новая игра";
            this.newGame.UseVisualStyleBackColor = true;
            this.newGame.Click += new System.EventHandler(this.newGame_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 191);
            this.Controls.Add(this.newGame);
            this.Controls.Add(this.countLabel);
            this.Controls.Add(this.numberNow);
            this.Controls.Add(this.numberLabel);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.multiButton);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button multiButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Label numberLabel;
        private System.Windows.Forms.Label numberNow;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.Button newGame;
    }
}