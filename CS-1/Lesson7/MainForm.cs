﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson7
{
    public partial class MainForm : Form
    {
        Game myGame;
        public MainForm()
        {
            InitializeComponent();
            myGame = new Game(0, 100);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            numberLabel.Text = myGame.mainCount.ToString();
            countLabel.Text = myGame.count.ToString();
            numberNow.Text = myGame.currentCount.ToString();
        }

        private void newGame_Click(object sender, EventArgs e)
        {
            myGame = new Game(0, 100);
            numberLabel.Text = myGame.mainCount.ToString();
            countLabel.Text = myGame.count.ToString();
            numberNow.Text = myGame.currentCount.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            myGame.Plus();
            countLabel.Text = myGame.count.ToString();
            numberNow.Text = myGame.currentCount.ToString();
            if (myGame.mainCount == myGame.currentCount)
            {
                MessageBox.Show("Победа");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            myGame.Multi();
            countLabel.Text = myGame.count.ToString();
            numberNow.Text = myGame.currentCount.ToString();
            if (myGame.mainCount == myGame.currentCount)
            {
                MessageBox.Show("Победа");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            myGame.ResetGame();
            countLabel.Text = myGame.count.ToString();
            numberNow.Text = myGame.currentCount.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            myGame.Back();
            countLabel.Text = myGame.count.ToString();
            numberNow.Text = myGame.currentCount.ToString();
        }
    }
}
