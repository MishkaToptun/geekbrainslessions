﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson7
{
//2. Используя Windows Forms, разработать игру «Угадай число». Компьютер загадывает число от 1 до 100, а человек пытается его угадать за минимальное число попыток.Компьютер говорит, больше или меньше загаданное число введенного.
//a) Для ввода данных от человека используется элемент TextBox;
//б) ** Реализовать отдельную форму c TextBox для ввода числа.
//Старайтесь разбивать программы на подпрограммы.Переписывайте в начало программы условие и свою фамилию.Все программы сделать в одном решении.
//В свойствах проектах в качестве запускаемого проекта укажите “Текущий выбор”
    public partial class FormAnswers : Form
    {
        /// <summary>
        /// Загадываемое компьютером число
        /// </summary>
        int answerNumber = 0;
        public void NewGame()
        {
            answerNumber = new Random().Next(0, 100);
        }
        public void Chek()
        {

        }
        public FormAnswers()
        {
            InitializeComponent();
            NewGame();
        }

        private void FormAnswers_Load(object sender, EventArgs e)
        {

        }

        private void testBut_Click(object sender, EventArgs e)
        {
            AnswerForm form = new AnswerForm(this);
            form.Show();
        }
        public void TestAnswer(string answer)
        {
            int res;
            bool test = int.TryParse(answer, out res);
            if (test == false)
            {
                MessageBox.Show("Неверный вид ответа");
            }
            else
            {
                if (res < answerNumber)
                {
                    MessageBox.Show("Меньше");
                }
                if (res > answerNumber)
                {
                    MessageBox.Show("Больше");
                }
                if (res== answerNumber)
                {
                    MessageBox.Show("Угадал!");
                }
            }
        }

        private void newValue_Click(object sender, EventArgs e)
        {
            NewGame();
        }
    }
}
