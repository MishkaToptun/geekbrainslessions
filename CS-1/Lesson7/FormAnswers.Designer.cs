﻿
namespace Lesson7
{
    partial class FormAnswers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newValue = new System.Windows.Forms.Button();
            this.testBut = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newValue
            // 
            this.newValue.Location = new System.Drawing.Point(12, 13);
            this.newValue.Name = "newValue";
            this.newValue.Size = new System.Drawing.Size(95, 25);
            this.newValue.TabIndex = 0;
            this.newValue.Text = "Загадать новое";
            this.newValue.UseVisualStyleBackColor = true;
            this.newValue.Click += new System.EventHandler(this.newValue_Click);
            // 
            // testBut
            // 
            this.testBut.Location = new System.Drawing.Point(13, 52);
            this.testBut.Name = "testBut";
            this.testBut.Size = new System.Drawing.Size(93, 23);
            this.testBut.TabIndex = 1;
            this.testBut.Text = "Попытаться";
            this.testBut.UseVisualStyleBackColor = true;
            this.testBut.Click += new System.EventHandler(this.testBut_Click);
            // 
            // FormAnswers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 96);
            this.Controls.Add(this.testBut);
            this.Controls.Add(this.newValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormAnswers";
            this.Text = "Угадай число";
            this.Load += new System.EventHandler(this.FormAnswers_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newValue;
        private System.Windows.Forms.Button testBut;
    }
}